Feature: Add users and roles
  In order to delegate content publishing
  As a site builder
  I need to add roles and users

  Background:
  Given I am on "/"
  And I fill in "Username" with "admin"
  And I fill in "Password" with "alepc12"
  And I press "Log in"

  Scenario: Move Cathy's work to needs review
    When I follow "Log out"
    And I fill in "Username" with "Cathy Contributor"
    And I fill in "Password" with "cathy1"
    And I press "Log in"
    And I follow "Add content"
    And I follow "Article"
    And I fill in "Title" with "Contributor FAQ"
    And I press "Save"
    And I follow "My Workbench"
    And I press "edit"
    And I follow "Log out"
    And I fill in "Username" with "Eli Editor"
    And I fill in "Password" with "eli1"
    And I press "Log in"
    And I follow "My Workbench"
    And I follow "Needs Review"
