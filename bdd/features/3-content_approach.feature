Feature: Content approach
  In order to delegate content publishing
  As a site builder
  I need to create content to work with, create a page with newest article, and create an archive

  Background:
  Given I am on "/"
  And I fill in "Username" with "admin"
  And I fill in "Password" with "alepc12"
  And I press "Log in"

  Scenario: create content to work with
    When I follow "Configuration"
    And I follow "Generate content"
    And I check "Article"
    And I uncheck "Basic page"
    And I select "1 year ago" from "How far back in time should the nodes be dated"
    And I press "Generate"
