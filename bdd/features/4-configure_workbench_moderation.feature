Feature: Configure workbench moderation
  In order to delegate content publishing
  As a site builder
  I need to set the workbench moderation permissions

  Background:
  Given I am on "/"
  And I fill in "Username" with "admin"
  And I fill in "Password" with "alepc12"
  And I press "Log in"

  Scenario: Enable workflow
    When I follow "Structure"
    And I follow "Content types"
    And I follow "edit"
    And I check "Create new revision"
    And I check "Enable moderation of revisions"
    And I press "Save content type"

  Scenario: permissions core
    When I follow "People"
    And I follow "Permissions"
    And I check "contributor: Use the administration toolbar"
    And I check "editor: Use the administration toolbar"
    And I check "publisher: Use the administration toolbar"
    And I check "contributor: Access My Workbench"
    And I check "editor: Access My Workbench"
    And I check "publisher: Access My Workbench"
    And I check "editor: View all unpublished content"
    And I check "publisher: View all unpublished content"
    And I check "contributor: View moderation history"
    And I check "editor: View moderation history"
    And I check "publisher: View moderation history"
    And I check "contributor: View the moderation messages on a node"
    And I check "editor: View the moderation messages on a node"
    And I check "publisher: View the moderation messages on a node"
    And I check "contributor: Use \"My Drafts\" workbench tab"
    And I check "editor: Use \"My Drafts\" workbench tab"
    And I check "publisher: Use \"My Drafts\" workbench tab"
    And I check "editor: Use \"Needs Review\" workbench tab"
    And I check "publisher: Use \"Needs Review\" workbench tab"
    And I check "contributor: Moderate all content from Draft to Needs Review"
    And I check "editor: Moderate all content from Draft to Needs Review"
    And I check "publisher: Moderate all content from Draft to Needs Review"
    And I check "editor: Moderate all content from Needs Review to Draft"
    And I check "publisher: Moderate all content from Needs Review to Draft"
    And I check "editor: Moderate all content from Needs Review to Published"
    And I check "publisher: Moderate all content from Needs Review to Published"
    And I press "Save permissions"
