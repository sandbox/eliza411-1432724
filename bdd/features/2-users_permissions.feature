Feature: Users and permissions
  In order to delegate content publishing
  As a site builder
  I need to give permissions to users

  Background:
  Given I am on "/"
  And I fill in "Username" with "admin"
  And I fill in "Password" with "alepc12"
  And I press "Log in"

  Scenario: Users and permissions
    When I follow "People"
    And I follow "Permissions"
    And I check "contributor: View own unpublished content"
    And I check "contributor: View content revisions"
    And I check "contributor: Article: Create new content"
    And I check "contributor: Article: Edit own content"
    And I check "contributor: Basic page: Create new content"
    And I check "contributor: Basic page: Edit own content"
    And I check "editor: View own unpublished content"
    And I check "editor: View content revisions"
    And I check "editor: Revert content revisions"
    And I check "editor: Article: Create new content"
    And I check "editor: Article: Edit own content"
    And I check "editor: Article: Edit any content"
    And I check "editor: Basic page: Create new content"
    And I check "editor: Basic page: Edit own content"
    And I check "editor: Basic page: Edit any content"
    And I check "publisher: View own unpublished content"
    And I check "publisher: View content revisions"
    And I check "publisher: Revert content revisions"
    And I check "publisher: Article: Create new content"
    And I check "publisher: Article: Edit own content"
    And I check "publisher: Article: Edit any content"
    And I check "publisher: Basic page: Create new content"
    And I check "publisher: Basic page: Edit own content"
    And I check "publisher: Basic page: Edit any content"
    And I press "Save permissions"
