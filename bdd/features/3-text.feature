Feature: Test user permissions
  In order to delegate content publishing
  As a site builder
  I need to test user permissions

  Background:
  Given I am on "/"
  And I fill in "Username" with "admin"
  And I fill in "Password" with "alepc12"
  And I press "Log in"

  Scenario: Create an article as Eli Editor and Cathy Contributor
    When I follow "Log out"
    And I fill in "Username" with "Eli Editor"
    And I fill in "Password" with "eli1"
    And I press "Log in"
    And I follow "Add content"
    And I follow "Article"
    And I fill in "Title" with "Eli before workflow"
    And I press "Save"
    And I follow "Log out"
    And I fill in "Username" with "Cathy Contributor"
    And I fill in "Password" with "cathy1"
    And I press "Log in"
    And I follow "Add content"
    And I follow "Article"
    And I fill in "Title" with "Cathy before workflow"
    And I press "Save"
    And I follow "Home"
    And I follow "Eli before workflow"
    Then I should not see the link "Edit"

  Scenario: Verify that Peggy can add articles/pages and can edit eli's post
    When I follow "Log out"
    And I fill in "Username" with "Peggy Publisher"
    And I fill in "Password" with "peggy1"
    And I press "Log in"
    And I follow "Add content"
    And I should see "Article"
    And I should see "Basic page"
    And I follow "Home"
    And I follow "Eli before workflow"
    Then I should see "Edit"
