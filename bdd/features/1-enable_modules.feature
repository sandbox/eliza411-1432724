Feature: Enable modules
  In order to delegate content publishing
  As a site builder
  I need to Enable modules

  Background:
  Given I am on "/"
  And I fill in "Username" with "admin"
  And I fill in "Password" with "alepc12"
  And I press "Log in"

  Scenario: Enable modules needed
    When I follow "Modules"
    And I check "Chaos tools"
    And I check "Views"
    And I check "Views UI"
    And I check "Date"
    And I check "Date Popup"
    And I check "Scheduler"
    And I check "Diff"
    And I check "Devel"
    And I check "Devel generate"
    And I press "Save configuration"
