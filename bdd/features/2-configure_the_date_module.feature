Feature: Configure the date module
  In order to delegate content publishing
  As a site builder
  I need to configure regional settings and configure date and time

  Background:
  Given I am on "/"
  And I fill in "Username" with "admin"
  And I fill in "Password" with "alepc12"
  And I press "Log in"

  Scenario: configure regional settings
    When I follow "Configuration"
    And I follow "Regional settings"
    And I select "United States" from "Default country"
    And I select "Sunday" from "First day of week"
    And I uncheck "Users may set their own time zone"
    And I press "Save configuration"

  Scenario: configure date and time
    When I follow "Configuration"
    And I follow "Date and time"
    And I press "Save configuration"
